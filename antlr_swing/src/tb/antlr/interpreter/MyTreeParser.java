package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	private GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer getVar(String name) {
		return globalSymbols.getSymbol(name);
	}
	
	protected Integer setVar(String name, Integer value) {
		globalSymbols.setSymbol(name, value);
		return value;
	}
	
	protected void newVar(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected Integer dodaj(Integer e1, Integer e2) {
		return e1 + e2;
	}
	
	protected Integer odejmij(Integer e1, Integer e2) {
		return e1 - e2;
	}
	
	protected Integer pomnoz(Integer e1, Integer e2) {
		return e1 * e2;
	}
	
	protected Integer podziel(Integer e1, Integer e2) {
		return e1 / e2;
	}
}