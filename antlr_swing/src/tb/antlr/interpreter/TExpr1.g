tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());} | decl)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = dodaj($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = odejmij($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = podziel($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setVar($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVar($ID.text);}
        ;

decl 
        : ^(VAR   i1=ID)           {newVar($i1.text);}
        ;

